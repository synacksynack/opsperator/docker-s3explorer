FROM opsperator/apache

# S3 Explorer image for OpenShift Origin

ARG DO_UPGRADE=

LABEL io.k8s.description="S3 Buckets Explorer" \
      io.k8s.display-name="S3 Explorer" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="s3,bucket,explorer" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-s3explorer" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0"

USER root

COPY config/* /usr/share/s3explorer/

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && mv /usr/share/s3explorer/run-s3explorer.sh \
	/usr/share/s3explorer/vhost.conf / \
    && mkdir -p /usr/share/s3explorer/fonts \
	/usr/share/s3explorer/webfonts \
    && mv /usr/share/s3explorer/glyphicons* /usr/share/s3explorer/fonts \
    && mv /usr/share/s3explorer/fa-solid* /usr/share/s3explorer/webfonts \
    && if test "$DO_UPGRADE"; then \
	echo "# Updatng Base Image" \
	&& apt-get -y update \
	&& apt-get -y upgrade \
	&& apt-get -y dist-upgrade; \
    fi \
    && echo "# Cleaning Up" \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/etc/ldap/ldap.conf \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-s3explorer.sh"]
USER 1001
