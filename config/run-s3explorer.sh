#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/nsswrapper.sh

export APACHE_DOMAIN=${APACHE_DOMAIN:-s3explorer.demo.local}
export APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
if test -z "$PUBLIC_PROTO"; then
    if test "$SSL_INCLUDE" = no-ssl; then
	PUBLIC_PROTO=http
    else
	PUBLIC_PROTO=https
    fi
fi
export APACHE_IGNORE_OPENLDAP=yay
export PUBLIC_PROTO
export RESET_TLS=false

if ! ls /etc/apache2/sites-enabled/*.conf >/dev/null 2>&1; then
    echo Generates VirtualHost Configuration
    sed -e "s|APACHE_DOMAIN|$APACHE_DOMAIN|" \
	-e "s|APACHE_HTTP_PORT|$APACHE_HTTP_PORT|" \
	-e "s|SSL_INCLUDE|$SSL_INCLUDE|" \
	/vhost.conf >/etc/apache2/sites-enabled/003-vhosts.conf
fi

. /run-apache.sh
